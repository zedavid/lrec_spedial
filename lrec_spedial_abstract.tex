\documentclass[10pt, a4paper]{article}
\usepackage{lrec2006}
\usepackage{verbatim}
\usepackage{amsmath,epsfig,graphics}
\usepackage{amsfonts}
\usepackage{url}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{tabulary}
\usepackage{graphicx} 
\usepackage{tabularx}
\usepackage{slashbox}
\usepackage{hyperref}
\setlength{\textwidth}{6.5 in}
\setlength{\evensidemargin}{0.100 in}
\setlength{\oddsidemargin}{0.000 in}
\setlength{\textheight}{9.00 in}
\setlength{\topmargin}{ -0.45 in}
\renewcommand{\baselinestretch}{1.0}
\renewcommand{\topfraction}{1}
\renewcommand{\bottomfraction}{1}
\renewcommand{\floatpagefraction}{1}
\renewcommand{\textfraction}{1}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}

\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\thelog}{log}
\DeclareMathOperator*{\pmi}{PMI}
\DeclareMathOperator*{\ppmi}{PPMI}

\title{The SpeDial Datasets: \\ Datasets for Spoken Dialogue Systems Analytics}

\author{
  Jos\'{e} Lopes$^1$,Arodami Chorianopoulou$^2$\\
  Elisavet Palogiannidi$^2$, Helena Moniz$^3$, Alberto Abad$^{3,4}$\\
  Katerina Louka $^5$, Elias Iosif$^{6,7}$, Alexandros Potamianos$^{6,7}$\\
  $^1$ KTH Speech, Music and Hearing, Stockholm, Sweden \\
  $^2$ School of ECE, Technical University of Crete, Greece\\
  $^3$ INESC-ID, Lisboa, Portugal\\
  $^4$ Instituto Superior T\'{e}cnico, Universidade de Lisboa, Portugal\\
  $^5$ Voiceweb S.A., Greece\\ 
  $^6$``Athena" Research and Innovation Center, Greece\\
  $^7$School of ECE, National Technical University of Athens, Greece\\
  {\small \tt jdlopes@kth.se}\\
  %\{achorianopoulou,epalogiannidi\}@isc.tuc.gr}\\
  %{\tt klouka@voiceweb.eu,\{iosife,potam\}@central.ntua.gr}
  %\vspace*{-0.5in}
  }
  
\date{}

\begin{document}
\maketitle
\thispagestyle{empty}

\section{Introduction}
\label{s:intro}
The speech services industry has been growing both for telephony applications and, recently, also for
smartphones (e.g., Siri). Despite recent progress in Spoken Dialogue System
(SDS) technologies the development cycle of speech services still requires significant effort and
expertise. The SpeDial consortium (\url{www.spedial.eu}) is working to create a
semi-automated process for spoken dialogue service development and speech service enhancement of
deployed services, where incoming speech service data are semi-automatically
transcribed and analyzed (human-in-the-loop). The first step towards this goal
was to build tools that automatically identify problematic dialogue situations
or as we will call hereafter miscommunications.

The automatic detection of miscommunications in SDSs has been extensively
investigated in the literature
\cite{Walker:2000:LPP:974305.974333,PaekH04,SchmittSMLS10,SwertsLH00}. This
problem is vital in the development cycle of speech services. However, very
little data is publicly available to perform research on this topic. Except
for \cite{SwertsLH00} the data is not publicly available.
Nevertheless, even in this case the dataset does not contain interactions with
real users or annotations. The LEGO corpus \cite{SCHMITT12.333} included both
interactions with real users and annotations for interaction quality
\cite{schmitt2011a}, emotions and other automatically extracted features. This
corpus was based CMU Let's Go system from 2006, whose performance is
substantially worse than current Let's Go system.
In addition, the interaction quality might not be the most suitable measure for
identifying problematic dialogue situations, namely if severe problems occur in
the very first exchange of the interaction. Therefore, we decided to look for more
recent data and work on a new annotation scheme (introduced in Section
\ref{sec:scheme}).

We are making two datasets publicly available. The first from Let's Go
\cite{raux101744} collected during 2014 and a dataset from a Greek Movie
Ticketing (MT) system. The datasets were used to build several detectors that
could help us on the one hand to analyze the users and their behavior (such as
age and task success), and on the other hand could serve as inputs to improve
miscommunication detection (such anger). Therefore, both corpora will be
distributed with manual transcriptions for every user turn, together with gender,
task success, anger and miscommunication annotations.

% To achieve our goal these datasets have been manually annotated in several
% dimensions. Here we are presenting only those that are common to both corpora.
% At the dialogue level gender and task success, and at the turn level anger,
% miscommunications, bargeins and transcriptions. The datasets include the
% SpeDial proprietary XML file (one for each dataset) and audios from the user
% turns.
% 
% These datasets were used to train several classifiers that were developed in the
% scope of the project. In this abstract we will present preliminary results for
% gender and anger detectors. We also have preliminary results for the
% miscommunication detector that in the future will benefit from the input of
% anger, gender and others detectors under development to reduce the human
% intervention in the loop.

So far, the results for stand-alone classifiers for anger, gender and
miscommunication are very promising, proving the usefulness of the
datasets that we are releasing for future research in SDS Analytics.

\begin{table*}[t]
\centering
\scriptsize
\begin{tabular}{|c|c|c|}
\hline
Annotation & Turn Id & Turn \lbrack TRANSCRIPTION, Parse \rbrack \\
\hline
& S1 & Where would you like to leave from? \\
\hline
& U2 & WEST MIFFLIN  \lbrack WEST MIFFLIN AND, DeparturePlace =
MIFFLIN\rbrack\\
\hline 
NOT PROBLEMATIC & S3 & Departing from MIFFLIN. Is this correct? \\
\hline
& U4 & SHADY EIGHT \lbrack EXCUSE ME, (DeparturePlace = EIGTH,
DeparturePlace = SHADY)\rbrack\\
\hline
\end{tabular}
\caption{Example when label 3 was attribute to turn S3 in Let's Go data.}
\label{tab:Miscommunication}
\end{table*}


\section{Annotation scheme}
\label{sec:scheme}

The first step prior to annotate the data was to manually
transcribe the user utterances in both datasets. The system prompts in the MT
dataset were also transcribed since no system logs were available.

To perform the miscommunication annotation on Let's Go data, annotators were
given snippets of four turns, two system and two user turns. Recognition output
and transcription are presented to the annotator when performing the task. The
annotators had access to the audio from the utterances when they were
annotating.
The annotators task was to evaluate if the second
system turn is problematic or not based only on these turns. Label
$0$ was used when system answer was not considered problematic, $1$ when the
system answer was problematic and $2$ when the annotator could not decide from
the context whether the system answer was problematic or not. An example of a
snippet provided for annotation is shown in Table \ref{tab:Miscommunication}.
In the MT data annotation the annotator performed a similar task but using the
whole dialogue.



Along with the miscommunication annotation, annotators had to listen to the
utterance audio file and identify if anger was present. In Let's Go $1$ was used
when anger was detected and $0$ otherwise. The labels used in the Movie
Ticketing data were discrete scores that lie in the $[1-5]$ interval capturing
very angry user utterances ($1$) to friendly utterances ($5$). In order to adopt
the same scheme between corpora the values in the interval $[1-3]$ were mapped into $1$ and
values 4 and 5 were mapped into $0$.
 
While listening to the dialogue the annotators were asked to be aware
of gender. As soon as they were confident they would assign the gender label to
the whole dialogue. 

Finally, to annotate task success, the annotators should listen to the whole dialogue
and verify that if the intention of the user was correctly answered by the
system. The label $1$ was used for successful dialogues and the $0$ for
unsuccessful dialogues.

\section{Datasets}
\label{s:datasets}

\subsection{Let's Go}
\label{ss:datasets_letsgo}

This part of the dataset is composed of $85$ dialogues between real users and
the Let's Go Dialogue system. Iniatially $105$ dialogues were
randomly selected from dialogues collected during the first half of 2014. Dialogues
shorter than $4$ turns were then excluded from the dataset since this is the
minimum number of turns needed to get schedule information. The final $85$
dialogues correspond to $1449$ valid user turns (average $17.1$ turns per
dialogue).

The corpus was annotated following the scheme described in Section \ref{sec:scheme} for
Let's Go data. The dataset was enhanced with features from ASR,
Audio Manager, Dialogue Manager, Spoken Language Understanding and the
estimated task success extracted from system logs. Some features derived from
transcription and its parsing were also included, such as Word Error Rate and
Concept Error Rate.

The dataset was annotated by two expert annotators. One of them completely
annotated the corpus, whereas the other annotated 10\% of it. The Cohen's Kappa
agreement observed for the two annotators was $0.79$ for miscommunication
(substantial agreement), $0.38$ for anger (fair agreement), $1.0$ for task
success and $1.0$ for gender annotations (perfect agreement). We have computed
the agreement between the majority annotation for task success and the estimated
task success. The Cohen's kappa found was $0.44$, which is seen as fair
agreement.

\subsection{Movie ticketing}
\label{ss:datasets_movie}
The movie ticketing dataset consists of 200 dialogues in Greek
collected through a call center service for retrieving information about movies/showtimes
and booking tickets.
% The dataset includes two data types for each dialogue: 
% 1) audio recordings, and 2) the respective transcriptions. 
The annotation of dialogues was performed by an expert annotator,
while the selected dialogues were balanced with respect to three factors:
(i) gender of caller, (ii) call success, (iii) emotional content.
%In particular, the annotator listened the dialogues,
%transcribed the audio recordings, and annotated the user utterances
%with respect to  the annotations types described below.
% \newline
% \textbf{Emotion labels.}
% Three emotion annotation types are provided, namely,
% \emph{valence}, \emph{arousal}, and \emph{anger}.
% Valence and arousal annotations are provided both on utterance and dialogue level.
% Valence labels are discrete scores ranging from $-5$
% (for utterances/dialogues conveying extremely negative emotions)
% to $5$ (for utterances/dialogues characterized by extremely positive emotions).
% Arousal labels are discrete scores ranging
% from $0$ (for utterances/dialogues with extremely low activation)
% to $10$ (for utterances/dialogues with extremely high activation).
% Anger annotations are discrete scores that lie in the $[1-5]$ interval
% indicating very angry user utterances ($1$) to friendly (i.e., no anger) utterances ($5$).
% The anger scores are provided at the utterance level.
% \newline
% \textbf{Miscommunication labels.}
% Miscommunication labels are discrete scores that lie in the $[1-3]$ interval.
% The system prompts were annotated in order to indicate
% whether the system response was correct ($3$), partially correct ($2$) or wrong ($1$).
% This was done by considering the overall dialogue flow.

To verify the quality of annotations, two other annotators labeled a subset of
60 dialogues from the original dataset for anger. The agreement between
annotators found was $0.58$ with $0.4$ Kappa value
--computed as the average pairwise agreement--
according to the Fleiss coefficient.
%\footnote{These scores can be interpreted as a reasonable agreement.}.

\section{Results}
In this section, we briefly present a series of indicative experimental results
for a variety of different tasks. For anger and miscommunication detection a
leave-dialogue-out validation procedure was adopted. The results are reported in
terms of Unweighted Average Recall (UAR) since there is often a bias to one of
the classes. 
For the gender detection task, results are reported in terms of Accuracy for the complete set of turns from both datasets.
%, given that the gender models have been trained with external data sources.

\subsection{Anger detection in Movie Ticketing dataset}
\label{sss:movie_res}
The experimental results for the movie ticketing dataset are briefly presented 
with respect to two different systems performing
speech-- and text--based analysis.
\newline
\textbf{Speech-based system.}
Here, the aim is to capture the speaker's emotional state
via the utilization of a set of  low-level descriptors (LLDs)
including prosody (pitch and energy), short-term spectral and voice quality features \cite{verve04}.
The  LLDs  can be further exploited
via the application of a set of functionals, in order to map the speech contours to feature vectors. 
OpenSmile is a widely-used toolkit that can be used for extracting such features \cite{open10}.
A detailed system description is provided in \cite{spedial}.
% \footnote{The model will be further described (e.g., feature extraction) in
% the full version of the paper.}
\newline
\textbf{Text-based system.}
The goal is to estimate the emotional content of the transcribed speaker utterances.
A word $w$ can be characterized regarding its affective content in a continuous space
consisting of three dimensions, namely, valence, arousal, and dominance.
For each dimension, the affective content of $w$ is estimated as 
a linear combination of its' semantic similarities to a set of $K$ seed words 
and the corresponding affective ratings of seeds \cite{turn}. %\footnote{The
% model will be further described (e.g., seed selection, similarity computation, etc.) in the
% full version of the paper.}.
A detailed system description can be found in \cite{palogiannidi2015}.
%as follows
%\cite{turn,palogiannidi2015}:
%\begin{equation}
%\label{eq:aff}
%\hat{u}(w) = \lambda_0 + \sum_{i=1}^{K} \lambda_i \; u(t_i) \;S(t_i,w),
%\end{equation}
%\noindent
%where $t_1...t_K$ are the seed words,
%$u(t_i)$ is the affective rating for seed word $t_i$ with $u$ denoting
%one of the aforementioned dimensions, i.e., $v$, $a$, or $d$.
%$\lambda_i$ is a trainable weight corresponding to seed $t_i$.
%$S(t_i,w)$ stands for a metric of semantic similarity between $t_i$ and $w$.
For each speaker utterance, the valence, arousal, and dominance scores are computed for the respective words.
The statistics of these scores (e.g., mean, median, variance, etc. ) can be used as features.
%
%
%This system is an extension of \cite{turn} that was first proposed in \cite{maljou}.
%The cross-lingual application of this system was  investigated in \cite{palogiannidi2015}.
%The core idea is that
%a manually annotated affective lexicon is exploited for bootstrapping the estimation of affective ratings for unknown words.
%The underlying hypothesis is 
%that the affective rating of an unknown word can be estimated via
%the linear combination of the affective ratings of the seed words that are included in the lexicon
%and the semantic similarity between the seeds and the unknown word:
%\begin{equation}
%\label{eq:aff_text}
%\hat{\upsilon}(w_j) = a_0+ \sum_{i=1}^{N}a_i\upsilon(w_i)S(w_j,w_i),
%\end{equation} 
%where $w_j$ is the unknown word, $w_{1..N}$ are the seed words,
%$\upsilon(w_i)$ and $a_i$ are the affective score and the weight that correspond to the seed $w_i$.
%The semantic similarity between two words is denoted with the $S(\cdot)$. 
%The $a_i$ weights can be estimated through linear regression as described in \cite{maljou}.
%For each utterance/dialogue the valence, arousal, and dominance scores are computed for the respective words.
%The statistics of these scores (e.g., mean, median, variance, etc. ) are used as features.
\newline
\textbf{Experiments and evaluations results.}
The goal is the detection of ``angry'' vs. ``not angry''
(i.e., 2-class classification problem) user utterances.
For this purpose, the anger annotations were used.
% Specifically, the \textit{friendly} and \textit{neutral} labels were mapped to the ``not angry'' class,
% while the \textit{slightly angry}, \textit{angry} and \textit{very angry} labels were mapped to the ``angry'' class.
% For both speech-- and text--based systems,
% the evaluation was performed on the utterance level adopting the leave-one-dialogue-out process.
% The unweighted average recall (UAR) was used as evaluation metric.
% For the speech--based system the used feature set consisted of
% the first ten Mel-frequency cepstral coefficients (MFCCs) \cite{lee04} extracted via OpenSmile,
% while the statistics of dominance scores were used as features for the text--based system.
The best performance
obtained by the speech-- and text--based systems
equals to $0.67$ and $0.61$ UAR, respectively
%\footnote{Currently, for a subset of the dataset.
%In addition, the performance of the text--based system improves to 69\%
%when using an evaluation benchmark created by considering the text modality
% (i.e, transcriptions) instead of speech.} 
for the MT data,
exceeding the performance of majority--based classification regarded as naive
baseline ($0.5$ UAR for binary problems).
These performance scores were achieved by different classifiers,
JRip for speech and Random Forest for text.
%\footnote{A comparison between the different modalities
%(i.e., speech and text)
%and the respective performance yielded by various classifiers will be provided
% in the full version of the paper.}.
The affective speech analysis was also applied over the Let's Go dataset
for the task of anger detection achieving $0.88$ UAR. The attempts to use the
affective text analysis on Let's Go were in vain, since only 3 utterances in the
whole corpus include lexical anger markers.
%
%
%
%\begin{table}[!htb]
%\centering
%\begin{tabular}{|c|c|c|c|}
%\hline
%System & Chance & Weighted Recall \\
%\hline
%\hline
%\hline
%Text-based & 80.6 & 84.3 \\
%\hline
%Speech-based & 59.5 & 67.8 \\
%\hline
%\end{tabular}
%\caption{Movie ticketing dataset: ``angry'' vs. ``not angry'' classification}
%\label{tab:movies}
%\end{table}
%
%\begin{table}[!htb]
%\centering
%\begin{tabular}{|c|c|c|c|}
%\hline
%System & Chance & Weighted Recall \\
%\hline
%\hline
%\hline
%Speech-based & 95.0 & 97.2 \\
%\hline
%\end{tabular}
%\caption{Let'sGo dataset: ``shouting'' vs. ``not shouting'' classification}
%\label{tab:letsgo}
%\end{table}
%
%Both the affective speech and text analysis are performed on the utterance level,
%adopting the leave-one-dialogue-out process. 
%The best performance\footnote{ Due to space limitations.
%Random forests and JRip were used
%by the text-- and speech--based systems, respectively.}
%is reported in terms of \textit{weighted recall} in the Table \ref{tab:movies}, while our chance classifier assigns each test 
%%sample to the majority class.
%
%Text modality is very special in spoken dialogue data in the sense that they contain very 
%short, domain dependent (containing many name entities such as movies) or routing utterances. So, the anger detection from text a hard 
%problem and in many cases the affective text system cannot be applied (if not even humans are able to distinguish angry vs. not angry 
%utterances). In order to use the affective text system, necessary filters were applied to the user utterances in order to create a 
%reasonable subset. Specifically, very short, call routing and utterances with movie names were ignored, ending up in a subset that 
%consists of about 200 utterances. Note that for text system the user utterances were annotated again, based only on the text modality 
%(Results using annotations based on speech were also obtained and the performance was worse). 
%Best results for text achieved for Random 
%Forest classifier, using Valence and Arousal features and they are listed in the table ~\ref{tab:movies}. SVM and JRIP were also used, 
%achieving worse performance. 
%It is worth to note, that for utterances that the anger is expressed clearly through text (manually created 
%subset of 90 utterances) the performance of the proposed system increases from 50\% to 80\%.
%
%Regarding the speech affect analysis we present results for the 200 filtered utterances from the text modality. 
%For this experiment we used as features only the MFCCs given that the size of the data, i.e., 200 utterance, is too small.
%However, the classification accuracy for the full dataset, i.e., 200 dialogues, is estimated to $60.0\%$. 
%The best performance was achieved using a JRip classifier, while Random Forest yielded moderate results.
%
%Affective speech analysis was employed on the Let'sGo dataset as well, even though no anger annotation was provided. Hence, as annotation
%we selected the ``shouting'' label, which was provided in a binary scheme. For our experiments we used the leave-one-dialogue-out 
%technique and 2 features, namely energy and the first mel-frequency coefficient.  
%Best results were achived by using a JRip classifier.
%Table \ref{tab:letsgo} presents the evaluation results with respect to weighted recall.

\subsection{Gender detection in Let's Go and Movie Ticketing datasets}
The Gender detector mainly consists of a multi-layer perceptron (MLP) trained with $\sim$70 hours of multi-lingual telephone speech data. Accuracies obtained are 91.7\% and 89.7\% in the Let's Go and Movie Ticketing datasets, respectively. Thus, the module seems to perform consistently in both datasets, independently of the  language (notice that Greek data was not included in any of the MLP training sets). Although we consider these results quite promising, particularly considering the reduced amount of actual speech in most of the turns, we are currently developing and evaluating an alternative system based on i-vectors.

\subsection{Miscommunication detection in Let's Go}

We performed miscommuncation detection for this corpus using the same approach
described in \cite{meena3995}, except that instead of 10-fold cross validation
we have performed leave-one-dialogue-out cross validation. 

Using the JRip classifier implemented in Weka \cite{hall101538} and the set of
features that combines bag of concepts features, features derived from SLU and
features derived from the utterance, both including on-line and off-line
features, we have obtained an Unweighted Average Recall (UAR) of $0.88$. 
The same performance was obtained in the Random Forest
classifier from sklearn toolkit \cite{scikit-learn}. The libSVM classifier
\cite{CC01a} achieved a $0.73$ UAR.

\section{Conclusions and Future Work}
The key contribution of this work is the creation of datasets\footnote{Details about the dataset access will be provided in the final version of the paper.}
in two languages that enable the investigation of various research tasks
in the area of spoken dialogue analytics.

Regarding the experimental results, detectors seem
to have very satisfactory performance in those tasks
that humans have higher agreement such as miscommunication and gender. 
% The result achieved for anger using acoustic features is very satisfactory, given the low agreement between annotators.
Also, the performance achieved for anger detection using acoustic features is quite satisfactory.

For the full version of this paper,
we plan to extend the annotations, incluing more data annotated by two or more annotators.
We are  also currently working to integrate more detectors to those
described in this abstract and we expect to report results in both
datasets in the final version of the paper.
 
\section{Acknowledgements}
The authors would like to thank Maxine Eskenazi and Alan W. Black for
facilitating the access to the 2014 Let's Go data, Fernando Batista for
helping with the gender detection results and Maria Vomva for annotating the MT
dataset.
This research is supported by the EU project SpeDial � Spoken Dialogue Analytics, EU grant \# 611396.

%\bibliographystyle{plain}
\bibliographystyle{lrec2006}
\bibliography{lrec_spedial}

\end{document}
