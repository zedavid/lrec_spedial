\documentclass[10pt, a4paper]{article}
\usepackage{lrec2006}
\usepackage{verbatim}
\usepackage{amsmath,epsfig,graphics}
\usepackage{amsfonts}
\usepackage{url}
\usepackage{amssymb}
\usepackage{multirow}
\usepackage{graphicx} 
\usepackage{tabularx}
\usepackage{slashbox}
\setlength{\textwidth}{6.5 in}
\setlength{\evensidemargin}{0.100 in}
\setlength{\oddsidemargin}{0.000 in}
\setlength{\textheight}{9.00 in}
\setlength{\topmargin}{ -0.45 in}
\renewcommand{\baselinestretch}{1.0}
\renewcommand{\topfraction}{1}
\renewcommand{\bottomfraction}{1}
\renewcommand{\floatpagefraction}{1}
\renewcommand{\textfraction}{1}
\newcommand{\argmax}{\operatornamewithlimits{argmax}}

\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\thelog}{log}
\DeclareMathOperator*{\pmi}{PMI}
\DeclareMathOperator*{\ppmi}{PPMI}

%%% e-notes
\newcommand{\enotesoff}{\long\gdef\enote##1{}}
%\newcommand{\enoteson}{\long\gdef\enote##1{\par\noindent\fbox{\parbox {0.9\textwidth}{{\large DRAFT.} \small
\newcommand{\enoteson}{\long\gdef\enote##1{\par\noindent\fbox{\parbox {0.47\textwidth}{{\large {\bf DRAFT}  \newline} \small
\scshape ##1}}\\[0.3ex]}}
\enotesoff
\enoteson


\title{Cognitively Motivated Multilingual Word Representations}

\author{
  Elias Iosif, Spiros Georgiladakis, and Alexandros Potamianos\\
  School of Electrical \& Computer Engineering, National Technical University of Athens, Greece \\
  {\tt \{iosife,sgeorgil,potam\}@central.ntua.gr}
  %\vspace*{-0.5in}
  }

\date{}

\begin{document}
\maketitle
\thispagestyle{empty}


\section{Introduction}
\label{sec:intro}
Distributional semantic models (DSMs) aim to represent the meaning of lexical entities
by extracting linguistic features from textual corpora.
Word-level representations are the basis for more complex tasks such as
phrase-- and sentence--level representation and similarity computation \cite{mitchell10,agirre12}
with various applications, e.g.,
paraphrase detection \cite{androutsopoulos10}, affective text analysis \cite{malandrakis13}.

We argue that the representation task should be driven
by the (empirically identified) fundamental properties of the human memory.
In this spirit, we propose a novel type of word--level DSMs
designed according to a dual--processing cognitive system \cite{kahneman13}
that is triggered by lexico--semantic activations that take place in short--term memory.
The proposed model is
is shown to perform better than mainstream and state-of-the-art models
for the problem of word similarity computation 
with respect to various datasets in three languages (English, German, and Greek).
We make publicly available the used corpora along with a set of tools,
as well as a large repositories of contextual vectors
for all three languages.

\section{Distributional Semantic Models}
\label{s:sim}
Two types of DSMs are described, which rely on the distributional hypothesis of meaning \cite{harris54}.
Following the terminology coined in \cite{baroni14cnt_vs_pred} we refer to them as
\emph{context-counting DSMs} and \emph{context-predicting DSMs}.
\newline
{\bf Context-counting DSMs (CDSMs).}
This type can be regarded as the mainstream paradigm of DSMs.
The core idea is that the representation of word meaning
is implemented by considering the context in which the word occurs.
A context window of size $2H\!+\!1$ words
is centered on the word of interest $w_i$ and the contextual features
that fall within the window are extracted.
A high-dimensional space is built
--also known as vector space model (VSM)--
where the value of each dimension is set according
to the co-occurrence counts of words and their contextual features.
The transformation of raw co-occurrence counts was found to improve
the properties (e.g., smoothness) and performance of DSMs.
Such transformations aim to encode feature saliency,
e.g., positive point--wise mutual information (PPMI) \cite{bullinaria07}.
High-dimensional words--features matrices constitute the major implementation of VSM.
The dimensionality of such representations can be reduced
by applying techniques such as Singular Value Decomposition (SVD).
A survey of CDSMs is provided in \cite{turney10}. 
\newline
{\bf Context-predicting DSMs (PDSMs).}
PDSMs have been recently proposed,
while their key difference compared to CDSMs deal with the
computation of contextual features.
For a given $H$, the task is the prediction of contextual features of word $w_i$,
instead of counting the co-occurrence of $w_i$ and its features \cite{benjio03,huang12,mikolov13a}.
This is achieved by  maximizing the training objective,
which incorporates word--feature conditional probabilities.
\newline
{\bf Similarity metrics.}
For both CDSMs and PDSMs the cosine of feature vectors is the most widely--used metric
for computing word semantic similarity.
In \cite{lin98,turney10}, more metrics are discussed. 
%
% COSINE SIMILARITY AND PPMI
%
%For every instance of a word of interest, $w_i$, in the corpus
%the $H$ words left and right of $w_i$ were used to formulate a feature vector $v_i$.
%For a given value of $H$
%the context-based semantic similarity
%between two words, $w_i$ and $w_j$, was computed as the cosine of their feature vectors:
%\begin{equation}
%\label{eq:cosine}
%s^{H}(w_i,w_j) = \frac{v_i . v_j}{\mid\mid v_i \mid\mid~\mid\mid v_j \mid\mid}.
%\end{equation}
%The elements of the feature vectors are set according
%to a weighting scheme based on the frequency of the features.
%The weighting scheme we use is based on the point-wise mutual information (PMI).
%The PMI between a word $w_i$ and the $k$--th feature of its vector $x_i$, $f_{i}^{k}$,
%is computed as shown in (\ref{eq:pmi}) \cite{church90}. 
%\begin{equation}
%\label{eq:pmi}
%\pmi(w_i,f_{i}^{k}) = -\thelog\frac{\hat{p}(w_i,f_{i}^{k})}{\hat{p}(w_i)\hat{p}(f_{i}^{k})},
%\end{equation}
%where
%$\hat{p}(w_i)$ and $\hat{p}(f_{i}^{k})$
%are the occurrence probabilities of $w_i$ and $f_{i}^{k}$ , respectively,
%while the probability of their co-occurrence (within the $H$ window size)
%is denoted by $\hat{p}(w_i,f_{i}^{k})$. 
%The corpus-based frequencies of lexical items (words or character n-grams) were used in order to compute the probabilities,
%according to maximum likelihood estimation. 
%The scores derived using PMI  lie in the $[-\infty,+\infty]$ interval. 
%In particular, we use the positive point-wise mutual information (PPMI),
%in order to bound the computed scores within the $[0,+\inf]$ interval. PPMI is
%a special case of PMI according to which the negative PMI scores are set to zero, based on the assumption 
% that the contextual features
%that exhibit negative PMI do not contribute to the estimation of similarity  much \cite{bullinaria07}.


\section{Activation-based DSMs (ADSMs)}
\label{s:adsm}
%
%\begin{figure*} [ht]
%\begin{minipage}[htb]{.40\linewidth}
%\centerline{ \includegraphics [height=3.75cm,width=2.25cm]{artwork_lrec2016/ADSM_OVERVIEW.eps}  }
%\begin{flushleft}
%(a)
%\end{flushleft}
%\end{minipage}
%\hfill
%\begin{minipage}[htb]{.50\linewidth}
%\centerline{ \includegraphics [height=3.75cm,width=2.75cm]{artwork_lrec2016/ADSM_EXAMPLE.eps} }
%\begin{flushright}
%(b)
%\end{flushright}
%\end{minipage} 
%\caption{Activation-based DSMs.
%(a) Overview of the two-layer model;
%(b) Example of similarity computation between ``forest'' and ``fruit'':
%computation of activations (layer 1), followed by their transformation into VSM and computation of cosine between vectors (layer 2).}
%\label{fig:adsms}
%\end{figure*}
In this section, a cognitively motivated type of DSMs is proposed,
which is termed \emph{activation-based DSMs}.
The underlying hypothesis is that the representation of a target word $w_i$
takes the form  of a set of semantically related attributes (also words)
that are activated when $w_i$ is processed \cite{rogers04}.
This hypothesis is justified by the psycholinguistic phenomenon of \emph{semantic priming}
according to which the presence of a word
facilitates the cognitive processing of another word \cite{collins75,mcnam_05}.
Given a geometrical representation where the words of a vocabulary $V$
are arranged according to their respective pairwise semantic distances (i.e., a space $G$),
the activated attributes of $w_i$
can be regarded as a sub-space $G_i$ (also referred to as \emph{activation area})
that represents the semantics of $w_i$.
Such areas are expected be of relatively small size (compared to $V$)
given the limited capacity\footnote{
The capacity is not fixed, since it depends on the cognitive task under consideration \cite{cowan01}.}
of human short--term memory \cite{cowan01}.
A number of cognitive factors
(e.g., semantic concreteness \cite{barber13}, visuospatial characteristics \cite{yao13})
play a role to the size of activation areas
that varies across words.

In this work, we advance some of the design principles
of the aforementioned DSMs (CDSMs and PDSMs)
by putting into a computational framework the core implications of activation areas.
For a word, $w_i$, the largest possible activation area, $G_i^{max}$, is determined 
and a fraction of it is considered as the respective activation area $G_i$.
The two main aspects of this consideration
--which are not directly encoded in CDSMs and PDSMs--
are:
1) the injection of heavy sparsity in word representations via the exploitation of small activation areas,
and
2) the conditioning of sparsity since the size of activations are not the same for all words.
Each activation area is formulated as a vectorial representation
enabling the construction of VSM
on top of which well-established similarity metrics, e.g., cosine of vectors, can be applied.
An advantageous characteristic of this construction
is the representational fusion of ADSMs with existing DSMs (see next section),
since VSM constitutes the most common implementation shared by the majority of those models.
Overall, the proposed model is a two-tier system meant for
activation and similarity computation
adopting the paradigm presented in \cite{iosif2015nle}. 
In the first tier, the activation area $G_i$ of target $w_i$ is computed
by identifying and filtering
the most similar vocabulary words to $w_i$ according to a similarity metric.
The second layer is used for the computation of semantic similarity
of word pairs based on their respective activations.

The theoretical foundations of the two-tier architecture
originate from a generic cognitive model that was empirically found
to apply to numerous semantic and behavioral tasks \cite{kahneman13}.
Given a stimuli (e.g., a word or any other experiential entity),
the first layer rapidly activates (at the expense of accuracy) a group of similar/related entities.
The refinement of relations that exist between the stimuli and the activated entities
is passed to a second layer with slower but more accurate responses.

\section{Fusion of Models}
\label{s:fusion}
Here, we briefly describe two  schemes for the fusion
of semantic models based on the assumption that different models
encode (at some extent) different information.
The first scheme constitutes an early fusion
since the semantic representations of a target word are concatenated into a single hybrid representation.
According to the second scheme, the similarity scores estimated by different models are linearly combined
(i.e., late fusion). 

{\bf Fusion of semantic representations (early).}
Given a word $w_i$, let $v_{i,l}$ be its semantic representation computed via model $l$.
Assuming that this representation can be encoded as a vector,
and that $M$ models are available, the hybrid representation of $w_i$,
denoted as $\hat{v}_i$, can be computed as:
\begin{equation}
\label{eq:fuse1}
\hat{v}_i = v_{i,1} \oplus v_{i,2} \oplus \dots \oplus v_{i,M},
\end{equation}
where $\oplus$ stands for the vector--concatenate operator.
Then, similarity between two words, $w_1$ and $w_2$, can be estimated
by exploiting their hybrid representations $\hat{v}_1$ and $\hat{v}_2$
and applying standard similarity metrics, e.g., by taking their cosine. 
Since the vectors in (\ref{eq:fuse1}) come from different models,
normalization should take place before applying the $\oplus$ operations.
In this work, we propose the use of $Z$ normalization \cite{cohen95}.
The normalized $v_{i,l}$ vector is defined as:
\begin{equation}
\label{eq:znorm}
v_{i,l}^{'} = \frac{v_{i,l}-\mu_{i,l}}{\sigma_{i,l}},
\end{equation}
where $v_{i,l}$ and $\sigma_{i,l}$ are the arithmetic mean and the standard deviation
of feature values of $v_{i,l}$, respectively,
\newline
{\bf Fusion of similarity scores (late).}
The similarity between two words, $w_1$ and $w_2$, denoted as $\hat{S}(w_1,w_2)$, is estimated as follows: 
\begin{equation}
\label{eq:fuse2}
\hat{S}(w_1,w_2) = \lambda_0 + \sum_{j=1}^{K} \lambda_j\;S_j(w_1,w_2),
\end{equation}
where $S_j(w_1,w_2)$ stands for the similarity between $w_1$ and $w_2$ computed via model $j$,
while $\lambda_j$ is a trainable weight corresponding to model $j$.


\section{Experimental Settings}
\label{s:exps}
{\bf Corpora creation.}
For each language a web-harvested corpus was created according to the following procedure.
Starting from a dictionary, a web search query was formulated for each dictionary entry
and sent to the Yahoo! search engine.
For each query, up to the $1.000$ top ranked results (document snippets) were retrieved.
The overall corpus was built by aggregating the retrieved snippets.
\newline
{\bf Context-counting DSMs (CDSMs).}
Several window sizes ($H\!=\!1,\ldots,5$) were applied for extracting the contextual features,
which were weighted according to the PPMI scheme (see Section~\ref{s:sim}).
The dimensions of words--features matrix were reduced by applying SVD.
We experimented with various numbers of latent dimensions.
Here, we report results for $300$ dimensions for which the best results were obtained\footnote{
A comparison regarding the effect of the number of SVD dimensions will be provided in the full version of this work.}.
\newline
{\bf Context-predicting DSMs (PDSMs).}
The \texttt{word2vec} tool was used \cite{mikolov13a,mikolov13c}.
We applied the CBOW approach as being more computationally efficient \cite{mikolov13b,baroni14cnt_vs_pred}
for $H\!=\!1,\!\ldots,\!5$.
The dimensions of the resulting words--features matrix was set to $300$ based on the above mentioned finding.
\newline
{\bf Activation-based DSMs (ADSMs).}
For a target word, $w_i$, the maximum size of its activation area, $N_i$,
was estimated as the number of the vocabulary words that co-occur with $w_i$ within sentence boundaries.
The size of the activation area (also referred to as relative activation size)
was computed by selecting the $\alpha\!N$ most similar (using PPMI) words to $w_i$,
for $\alpha$ ranging from $1\%$ to $100\%$.
For the computation of the feature vector of $w_i$ in the second layer,
context windows $H\!=\!1,\ldots,5$ were applied
and only those words that were included in its activation area were considered as valid features
weighted according to PPMI.
A low--dimensional variation of ADSMs  was also implemented
by applying SVD over the words--features matrix and retaining $300$ latent dimensions.
\newline
{\bf Fusion of similarity scores (late).}
The $\lambda$ weights of (\ref{eq:fuse2}) were computed using least squares estimation\footnote{
Almost identical results were obtained when applying other methods,
e.g., ridge regression.}.
\newline
{\bf Similarity metrics.}
For all the aforementioned DSMs, the cosine of feature vectors
was used for computing word similarity.
\section{Evaluation Datasets and Results}
\label{s:eval}
%{\bf Evaluation.}
The task of noun semantic similarity computation was used for evaluation purposes.
\begin{table} [!ht]
\centering
\begin{tabular}{|c|c|c|}
\hline
Language & Dataset & \# pairs\\
\hline
\hline
English & ws353 & 353 \\
(EN)   & \cite{finkelstein02} & \\
\hline
English   & men1K & 1000 \\
(EN) & men3K & 3000 \\
   & \cite{bruni14} & \\
\hline
German & ws350ge & 350\\
(GE) & \cite{leviant15} & \\
\hline
Greek & gr200 & 200 \\
(GR) & \cite{zervanou14} & \\
\hline
\end{tabular}
\caption{Evaluation datasets.} 
\label{tab:datasets}
\end{table}
We used the datasets presented in Table \ref{tab:datasets},
which deal with three languages, namely,
English, German, and Greek. 
The Spearman correlation coefficient against human ratings was used as evaluation metric.
\begin{figure*} [ht]
\begin{minipage}[htb]{.45\linewidth}
\centerline{ \includegraphics [height=5.25cm,width=7.5cm]{act_figs/ws1rel.cor.spearman.eps}  }
\begin{flushleft}
(a)
\end{flushleft}
\end{minipage}
%\hfill
\begin{minipage}[htb]{.45\linewidth}
%\centerline{ \includegraphics [height=5.0cm,width=6.25cm]{act_figs/ws1rel.svd.cor.spearman.eps} }
\centerline{ \includegraphics [height=5.25cm,width=7.5cm]{act_figs/ws5rel.cor.spearman.eps} }
\begin{flushright}
(b)
\end{flushright}
\end{minipage} 
%\begin{minipage}[htb]{.45\textwidth}
%\begin{center}
%\centerline{ \includegraphics [height=5.0cm,width=6.25cm]{act_figs/ws3rel.cor.spearman.eps} }
%\end{center}
%\begin{flushleft}
%(c)
%\end{flushleft}
%\end{minipage}
%\hfill
%\begin{minipage}[htb]{.45\textwidth}
%\centerline{ \includegraphics [height=5.0cm,width=6.25cm]{act_figs/ws3rel.svd.cor.spearman.eps} }
%\begin{flushright}
%(d)
%\end{flushright}
%\end{minipage}
%\begin{minipage}[htb]{.45\textwidth} % 
%\begin{center}
%\centerline{ \includegraphics [height=5.0cm,width=6.25cm]{act_figs/ws5rel.cor.spearman.eps} }
%\end{center}
%\begin{flushleft}
%(c)
%\end{flushleft}
%\end{minipage}
%\hfill
%\begin{minipage}[htb]{.45\textwidth}
%\%centerline{ \includegraphics [height=5.0cm,width=6.25cm]{act_figs/ws5rel.svd.cor.spearman.eps} }
%\%begin{flushright}
%(d)
%\end{flushright}
%\end{minipage}
%\begin{minipage}[htb]{.90\textwidth} % 
%\begin{center}
%\centerline{ \includegraphics [height=5.0cm,width=6.25cm]{act_figs/simVSrelrel.cor.spearman.eps} }
%\end{center}
%\begin{center}
%(e)
%\end{center}
%\end{minipage}
\caption{Performance of activation-based DSMs.
Correlation as a function of the $\%$ of the activation areas for context window sizes:
(a) $H\!=\!1$, (b) $H\!=\!5$.
Results are shown for datasets in 3 languages: English (EN), German (GE), and Greek (GR).}
%(e) similarity vs. relatedness wrt $H\!=\!1, 3, 5$ for English.}
\label{fig:cor}
\end{figure*}
The correlation scores\footnote{
Results for men3K only are shown, since men3K and men1K perform quite similarly.}
obtained by ADSMs as a function of the relative activation size
are presented in Figure~\ref{fig:cor}(a) and (b) for $H\!=\!1$ and $H\!=\!5$, respectively \footnote{
Results for $H\!=\!3$ are omitted as being the middle ground of $H\!=\!1$ and $H\!=\!5$.}.
It is observed that, when a narrow window size is used ($H\!=\!1$)
the best performance is obtained for activation sizes greater than $50\%$.
For $H\!=\!5$ the highest correlation is achieved for the $10\%$ of activations,
except for the case of the Greek dataset.
Overall, the highest correlation scores are achieved for the English datasets for all window sizes.
For the case of German and Greek the use of $H\!=\!5$ appears to yield higher performance than $H\!=\!1$.
The low-dimensional ADSMs (created by applying SVD with $300$ latent dimensions)
were found to yield significantly lower performance than ADSMs
across all datasets and languages.
%
\begin{table} [!ht]
\centering
\begin{tabular}{|c||c|c|c|c|}
\hline
Type & \multicolumn{4}{c|}{Relative activation size}\\
\cline{2-5}
 & 1\% & 3\% & 10\% & 20\% \\
\hline
\hline
%Similarity & $H\!=\!1$ & 0.43 & 0.63 & 0.73 & 0.64\\
\cline{2-5}
Similarity    &  0.63 & {\bf 0.77} & 0.74 & 0.68\\
\hline
%\hline
%Relatedness & $H\!=\!1$ & 0.31 & 0.45 & 0.62 & 0.74\\
\cline{2-5}
Relatedness &  0.59 & 0.72 & 0.70 & 0.62\\
\hline
\end{tabular}
\caption{Performance wrt similarity vs. relatedness.} 
\label{tab:simVSrel}
\end{table}
%
In Table~\ref{tab:simVSrel}, we present the performance for a number of relative activation sizes\footnote{
The performance drops for activations $>20\%$.}
(for $H\!=\!5$)
with respect to similarity and relatedness using
the respective subsets of word pairs  from the English dataset ws353 \cite{agirre09}.
The highest correlation score is obtained for the case of similarity.
The best scores for both similarity and relatedness are yielded by the $3\%$ of activations.
%\begin{table*} [ht]
%\centering
%\begin{tabular}{|c|c||c|c|c||c|}
%\hline
% &  & \multicolumn{4}{c|}{Type of DSMs}\\
%\cline{3-6}
%Language & Dataset & Context-counting & Context-predicting & Activation-based & Fusion\\
%\hline
%\hline
%%En & mc30 & 0.85 & 0.84 & & 0.86\\
%%\hline
%%En & rg65 & 0.78 & 0.82 & & 0.86\\
%%\hline
%EN & ws353 & 0.70 & 0.68 & 0.70 & {\bf 0.74}\\
%\hline
%EN & men1K & 0.76 & 0.74 &  0.78 & {\bf 0.80}\\
%\hline
%EN & men3K &  0.76 &  0.75 & 0.78 & {\bf 0.80}\\
%\hline
%GE & ws350ge & 0.56 & 0.60 & 0.63 & {\bf 0.66} \\
%\hline
%GR & gr200 & 0.58 & 0.53 & {\bf 0.72} & {\bf 0.72}\\
%\hline
%\end{tabular}
%\caption{Fusion of semantic representations: context-counting, context-predicting, and activation-based DSMs.} 
%\label{tab:fuse1}
%\end{table*}
%
%\begin{table*} [ht]
%\centering
%\begin{tabular}{|c|c||c|c||c|c|c||c|}
%\hline
%\multicolumn{2}{|c||}{Train} & \multicolumn{2}{|c||}{Test} & \multicolumn{4}{|c|}{Type of DSMs}\\
%\hline
%Lang & Dataset & Lang & Dataset & Context-counting & Context-predicting & Activation-based & Fusion \\
%\hline
%\hline
%%En & menTrain & En & mc30 & 0.85 & 0.84 & 0.77 &  {\bf 0.86}\\
%%\hline
%%En & menTrain & En & rg65 & 0.78 & 0.82 & 0.83 & {\bf 0.86}\\
%%\hline
%EN & menTrain & EN & ws353 & 0.70 & 0.68 & 0.70 & {\bf 0.75}\\
%\hline
%EN & menTrain & EN & men1K & 0.76 & 0.74 & 0.78 & {\bf 0.79}\\
%\hline
%EN & men3K & GE & ws350ge & 0.56 & 0.60 & {\bf 0.63} & 0.61\\
%\hline
%EN & men3K & GR & gr200 & 0.58 & 0.53 & {\bf 0.72} & 0.62\\
%\hline
%\end{tabular}
%\caption{Fusion of similarities: context-counting, context-predicting, and activation-based DSMs.} 
%\label{tab:fuse2}
%\end{table*}
%
%
\begin{table} [!ht]
\centering
\begin{tabular}{|c||c|c|c||c|}
\hline
Fusion &  \multicolumn{4}{c|}{Type of DSMs}\\
\cline{2-5}
type & CDSMs & PDSMs & ADSMs & Fused\\
\hline
\hline
Early & 0.67 & 0.66 & 0.72 & {\bf 0.74}\\
\hline
Late & 0.65 & 0.64 &  0.71 & 0.69\\
\hline
\end{tabular}
\caption{Average performance of early and late fusion.} 
\label{tab:fuse1and2avg}
\end{table}
The average performance of early and late fusion
is shown in Table~\ref{tab:fuse1and2avg}.
Context window $H\!=\!5$ was applied for all fused models (i.e., CDSMs, PDSMs, and ADSMs),
while the $10\%$ of the activation size was utilized for ADSMs.
For the case of early fusion the the performance is reported in terms of average correlation
considering the correlation scores obtained for all datasets
(ws353, men1K, men3K, ws350ge, and gr200).
The same averaging of correlation scores is also followed for the case of late fusion.
For the case of English datasets (ws353, men1K),
we used a subset of the largest dataset (men3K) for training purposes
(learning the $\lambda$ weights of (\ref{eq:fuse2}))
by excluding from it the pairs that are included in ws353 and men1K.
For the case of German and Greek datasets (ws350ge and gr200, respectively)
the entire men3K English dataset was used for training.
On average, the best correlation score (0.74) is achieved by the early fusion outperforming the individual models.
The proposed ADSMs are shown to obtain higher performance compared to CDSMs and PDSMs. 

\section{Corpora, Tools, and Resources}
\label{sec:resources}
\enote{
{\bf Corpora.}
The English, German, and Greek corpora are provided that consist of ???, ???, and ??? snippets, respectively.
}
\newline
{\bf Tools.}
The first tool is meant for corpus indexing and feature weighting,
while the second tool implements the activation-based DSMs.
\newline
{\bf Multilingual word vectors.}
For each language, a pool of vectors is provided
computed according to the following DSMs: (i) context counting, (ii) context-predicting, and (iii) activation-based.
100K vectors are made made available for each of the aforementioned DSMs.


\section{Conclusions}
\label{sec:concl}
The proposed activation-based DSMs were found to perform better than
the mainstream context-counting DSMs, as well as the recently proposed context-predicting DSMs.
This was observed for all datasets and languages.
Small activation sizes (i.e., $3-10\%$) contain adequate information for building semantic
representations in the framework of activation-based DSMs.
In addition, it was shown that the fusion of semantic representations (early fusion)
performs better than the fusion of similarity scores (late fusion).
A key operation regarding the fusion of representations is their normalization.
The $Z$ normalization was found to be relevant for this task
yielding performance that exceeds the individual models.
Last but not least, we make available multilingual resources (corpora and semantic representations) and tools.


\bibliographystyle{lrec2006}
\bibliography{refs}



\end{document}